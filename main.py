from kivy.app import App
from kivy.core.window import Window
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config
Config.set("graphics","width","1000")
Config.set("graphics","height","600")
Config.write()

#Window.size=(800,600)
class Container(BoxLayout):
    def __init__(self,**kwargs):
        BoxLayout.__init__(self,**kwargs)
        self.liste_button=[]
        self.orientation="vertical"
        self.spacing=30
        color=["red","blue","green","yellow","pink","brown"]
        self.inp=TextInput(hint_text='Ecrire Quelque chose Ici',font_size=20)
        self.add_widget(self.inp)
        self.lab = Label(text="",font_size=20)
        self.inp.bind(text=self.changetext)
        self.add_widget(self.lab)

        for i,j in enumerate(color):
            self.liste_button.append(Button(text=j,color="white",font_size=18,background_color=j,size_hint=(0.7,.6),pos_hint={"left":0.85,"right":0.85}))
            self.add_widget(self.liste_button[i])
            self.liste_button[i].bind(on_press=self.changecolor)


    def changetext(self,instance,v):
        self.lab.text=str(instance.text)
    def changecolor(self,instance):
        self.lab.color=instance.background_color

class Demo(App):
    def build(self):
        return Container()
Demo().run()